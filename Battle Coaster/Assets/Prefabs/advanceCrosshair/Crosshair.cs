﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


public class Crosshair : MonoBehaviour {
	
		[Range(1,20)]public float size,thickness;
		[Range(1,20)]public float gap;
		public bool outline;

		public Color color;
		public bool useDot;
		public bool dynamic;
		public bool autoUpdate;

		public List<UnityEngine.UI.Image> arrows; 
		public UnityEngine.UI.Image dot;

		float defaultSpread = 0f;
		public float resizedSpread = 30f;
		public float resizeSpeed = 6f;

		float spread;
		bool resizing = false;
	// Use this for initialization
		void Start () {
				spread = defaultSpread;
			if(notNullResources())
			{
				SetOptions();
			}
		}
	
		private bool notNullResources()
		{
				return (arrows != null && dot !=null);
		}

	// Update is called once per frame
		void Update () {

				if(Input.GetMouseButton(0))
				{ resizing = true; } else { resizing = false; }
				if (dynamic)
				{
						autoUpdate = false;
						if (resizing)
						{
								//increase spread 
								spread = Mathf.Lerp(spread, resizedSpread, resizeSpeed * Time.deltaTime);
						}
						else
						{
								//decrease spread
								spread = Mathf.Lerp(spread, defaultSpread, resizeSpeed * Time.deltaTime);
						}

						//clamp spread
						spread = Mathf.Clamp(spread, defaultSpread, resizedSpread);

				}
				DynamicArrows();
		}

		void LateUpdate()
		{
				if(autoUpdate)SetOptions();
		}

		void DynamicArrows()
		{
				arrows[0].rectTransform.localPosition = new Vector3(0,gap + spread,0);
				arrows[1].rectTransform.localPosition = new Vector3((gap * -1) - spread,0,0);
				arrows[2].rectTransform.localPosition = new Vector3(gap + spread,0,0);
				arrows[3].rectTransform.localPosition = new Vector3(0,(gap * -1) - spread,0);

		}

		void SetOptions()
		{
				dot.gameObject.SetActive(useDot);
				SetColor();

				arrows[0].rectTransform.sizeDelta = new Vector2(thickness,size);
				arrows[1].rectTransform.sizeDelta = new Vector2(size,thickness);
				arrows[2].rectTransform.sizeDelta = new Vector2(size,thickness);
				arrows[3].rectTransform.sizeDelta = new Vector2(thickness,size);

				arrows[0].rectTransform.localPosition = new Vector3(0,gap,0);
				arrows[1].rectTransform.localPosition = new Vector3(gap * -1,0,0);
				arrows[2].rectTransform.localPosition = new Vector3(gap,0,0);
				arrows[3].rectTransform.localPosition = new Vector3(0,gap * -1,0);
				//foreach (var arrow in arrows) {
				//		arrow.GetComponent<Outline>().enabled = outline;
				//}

		}

		void SetColor()
		{
				foreach (var arrow in arrows) {
						arrow.color = color;
				}
				dot.color = color;
		}

}
