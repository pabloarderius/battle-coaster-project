﻿using UnityEngine;
using UnityEngine.Networking;
using Invector.CharacterController;
using System.Collections;

public class PlayerHealth : NetworkBehaviour
{
    [SyncVar(hook = "OnChangeHealth")]
    public float health = 100;

    private bool isDead = false;

    private string lastHitPlayer = "";

    public Behaviour[] disableOnDeath;

    HealthBar healthBar;

    public GameObject ragdoll;

    public float _maxExplosionDamage = 150;
    private float _explosionRadius;

    private Player otherPlayer;
    private int otherPId;

    private void Start()
    {
        // healthBar = GameObject.Find("HealthBarUI").GetComponent<Image>();
        healthBar = FindObjectOfType<HealthBar>();
    }

    public void CalculateDmg(string playerIdHitted, string damageFromPlayerId, GameObject explosion, float explosionRadius)
    {
        lastHitPlayer = damageFromPlayerId;
        explosion.GetComponent<SphereCollider>().enabled = false;
        _explosionRadius = explosionRadius;
        float explDamage = CalculateDamageExplosion(explosion.transform.position);

        if (explDamage >= 1) //if damage is received
        {
            CmdReceiveDamage(playerIdHitted, damageFromPlayerId, explDamage);
        }
    }

    [Command]
    private void CmdReceiveDamage(string playerIdHitted, string damageFromPlayerId, float explDamage)
    {
        Debug.Log("hitted: " + playerIdHitted + " receibe damage by: " + damageFromPlayerId);
        //cojo el player del from seugn el gamemanager getplayerbyid y a ese player le añado kills ++
        if (isServer)
        {
            RpcTakeDamage(explDamage); //tell all I received damage
        }
    }

    [Command]
    public void CmdAddHealth(float healthAdded)
    {
        if (isServer)
        {
            RpcAddHealth(healthAdded); //tell all I received damage
        }
    }

    [ClientRpc]
    public void RpcAddHealth(float healthAdded)
    {
        health += healthAdded;
        if (health >= 100)
        {
            health = 100;
        }

        if (isLocalPlayer)
        {
            healthBar.ChangeHealthBar(health);
        }
    }

    [ClientRpc]
    private void RpcTakeDamage(float damage)
    {
        //if (GameManager.Instance.players.Count == 1)
        //{ return; } //Dont take damage if only is one player

        health -= (int)damage;

        if (isLocalPlayer)
        {
            healthBar.ChangeHealthBar(health);
        }

        if (health <= 0)
        {
            if (GameManager.Instance.players.Count == 1)
            {
                return; //Dont take damage if only is one player
            }
            health = 0;

            Death();
        }
    }

    [Command]
    private void CmdPlayerKill(string whoKilled)
    {
        Debug.Log(whoKilled + " killed: " + gameObject.name);
        Player playerK = GameManager.Instance.GetPlayer(whoKilled);

        //Add kills to player who killed
        PlayerSync otherPSync = playerK.GetComponent<PlayerSync>();
        int totalKills = otherPSync.m_playerKills + 1;
        otherPSync.CmdSetKills(totalKills);
    }

    private void Death()
    {
        if (isServer)
        {
            RpcDisableComponents();
        }
        //GameObject rag = Instantiate(ragdoll, new Vector3(transform.position.x, transform.position.y + 2.6f + transform.position.z), transform.rotation);
        //if (isLocalPlayer)
        //{
        //    transform.GetComponent<vThirdPersonInput>().cam.GetComponent<vThirdPersonCamera>().SetTarget(rag.transform);
        //}
        ////Spawn the ragdoll on the Clients
        //NetworkServer.Spawn(rag);       
    }

    [ClientRpc]
    private void RpcDisableComponents()
    {
        Debug.Log("Player dead");
        transform.GetChild(0).gameObject.SetActive(false); //Deactive skin
        transform.GetComponent<CapsuleCollider>().enabled = false; //Deactive player collider
        transform.GetComponent<Rigidbody>().isKinematic = true;

        foreach (var item in disableOnDeath)
        {
            item.enabled = false; //disable components
        }

        GameManager.Instance.UnRegisterPlayer(gameObject.name);

        if (isLocalPlayer)
        {
            string espectatingText = "";

            if (lastHitPlayer == "HIMSELF") //player hits himself
            {
                otherPlayer = GameManager.Instance.GetPlayerByDictionaryPos(0); //gets first or can get random player
                espectatingText = "You killed yourself bro...";
            }
            else
            {
                otherPlayer = GameManager.Instance.GetPlayer(lastHitPlayer); //change camera to player who killed you
                espectatingText = "Killed by " + "<color=red>" + otherPlayer.playerName + "</color>";
            }

            CmdPlayerKill(lastHitPlayer /*gameObject.name*/);

            UIManager.Instance.SetEspectatingText(espectatingText);
            UIManager.Instance.SetOtherPlayerKills(otherPlayer.GetComponent<PlayerSync>().m_playerKills);
            StartCoroutine(ChangeOtherPlayerCamera());
        }
    }

    private void Update()
    {
        if (isDead) //Change other players healthbar
        {
            if (otherPlayer == null || otherPlayer.GetComponent<PlayerHealth>().health <= 0) //if player watching is dead or has gone
            {
                //change to other ¿random? player
                otherPlayer = GameManager.Instance.GetPlayerByDictionaryPos(0); //gets first or can get random player
                Debug.Log("other player also dies:" + "new player is: " + otherPlayer.name);

                StartCoroutine(ChangeOtherPlayerCamera());
            }
            float otherPHealth = otherPlayer.GetComponent<PlayerHealth>().health;
            if (health != otherPHealth)
            {
                ChangeOtherPlayerHealthBar();
                health = otherPHealth;
            }
        }
    }

    private IEnumerator ChangeOtherPlayerCamera()
    {
        yield return new WaitForSeconds(2);
        if (isLocalPlayer)
        {
            isDead = true;

            Debug.Log("Change to: " + otherPlayer.playerName);

            Camera myCamera = transform.GetComponent<vThirdPersonInput>().cam.GetComponent<Camera>();
            Camera otherPCamera = otherPlayer.GetComponent<vThirdPersonInput>().cam.GetComponent<Camera>();

            myCamera.enabled = false;
            otherPCamera.enabled = true;

            ChangeOtherPlayerHealthBar();
        }
    }

    public void OnChangeHealth(float value)
    {
        if (health == value) return;
        health = value;
    }

    private void ChangeOtherPlayerHealthBar()
    {
        healthBar.ChangeHealthBar(otherPlayer.GetComponent<PlayerHealth>().health);
    }

    //Explosion
    private float CalculateDamageExplosion(Vector3 targetPosition)
    {
        Vector3 explosionToTarget = targetPosition - transform.position;

        float explosionDistance = explosionToTarget.magnitude;
        float relativeDistance = (_explosionRadius - explosionDistance) / _explosionRadius;
        float damage = relativeDistance * _maxExplosionDamage;
        damage = Mathf.Max(0, damage);
        return damage;
    }
}
