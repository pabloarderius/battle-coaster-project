﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Rotation : MonoBehaviour {

    public Transform weapon;
    public Transform cameraTarget;

    public Transform floatingLeftHand;
    public Transform floatingRightHand; //Targets for weapon (in player root)

    public Transform weaponLeftHand;
    public Transform weaponRightHand; //Inside weapon

    void Start () {
        floatingLeftHand.rotation = weaponLeftHand.transform.rotation;
        floatingRightHand.rotation = weaponRightHand.transform.rotation;
    }
	
	void LateUpdate () {
        //weapon.transform.rotation = cameraTarget.transform.rotation;

        if (cameraTarget == null || weapon == null) return;

        //weapon.transform.rotation = new Quaternion(cameraTarget.rotation.x, cameraTarget.rotation.y, cameraTarget.rotation.z, cameraTarget.rotation.w);
        LookAtPlayer();

        floatingLeftHand.position = weaponLeftHand.transform.position;
        floatingRightHand.position = weaponRightHand.transform.position;
    }

    private void WeaponLookAtCamera()
    {
        //weapon.transform.rotation = Quaternion.Slerp(weapon.transform.rotation, rotation, Time.deltaTime * 80f);

        weapon.rotation = new Quaternion(cameraTarget.rotation.x, weapon.rotation.y, cameraTarget.rotation.z, cameraTarget.rotation.w);

        //weapon.transform.LookAt(new Vector3(cameraTarget.position.x, weapon.transform.position.y, cameraTarget.position.z));

        //var lookPos = cameraTarget.position - weapon.transform.position;
        //lookPos.y = 0;
        //var rotation = Quaternion.LookRotation(lookPos);
        //weapon.transform.rotation = Quaternion.Slerp(weapon.transform.rotation, rotation, Time.deltaTime * 80f);
    }


    void LookAtPlayer()
    {
        weapon.transform.eulerAngles = new Vector3(cameraTarget.eulerAngles.x, weapon.eulerAngles.y, weapon.eulerAngles.z);

        //Vector3 dif = cameraTarget.position - weapon.position;
        //dif.x = weapon.position.x;
        //dif.z = 0;
        //Quaternion lookAngle = Quaternion.LookRotation(dif, transform.up);

        //weapon.rotation = lookAngle;

        //// get vector to player but take y coordinate from self to make sure we are not getting any rotation on the wrong axis
        //Vector3 socketLookAt = new Vector3(cameraTarget.position.x, weapon.position.y, cameraTarget.position.z);

        //// create rotations for socket and gun
        //Quaternion targetRotationSocket = Quaternion.LookRotation(socketLookAt - weapon.position);
        //Quaternion targetRotationGun = Quaternion.LookRotation(cameraTarget.position - weapon.position);

        //// slerp rotations and assign
        //weapon.rotation = Quaternion.Slerp(weapon.rotation, targetRotationSocket, Time.deltaTime * 50);
        //weapon.rotation = Quaternion.Slerp(weapon.rotation, targetRotationGun, Time.deltaTime * 50);

        //// important: reset local euler angles rotation of gun to make sure that we are getting rotation only on one axis
        //weapon.localEulerAngles = new Vector3(weapon.localEulerAngles.x, 0, 0);
    }

}
