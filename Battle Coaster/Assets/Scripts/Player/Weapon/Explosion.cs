﻿using UnityEngine;
using UnityEngine.Networking;

public class Explosion : NetworkBehaviour
{
    public string bulletFromPlayer = "";
    private const string PLAYERTAG = "Player";

    private void Start()
    {
        Destroy(gameObject, 3);
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageHit(other);
    }

    private void DamageHit(Collider other)
    {
        if (other.gameObject.CompareTag(PLAYERTAG))
        {
            if (other.GetComponent<Player>().isLocalPlayer)
            {
                if (other.name == bulletFromPlayer)
                {
                    //if player hits himself
                    //set bulletFromPlayer empty so after it can get other player
                    bulletFromPlayer = "HIMSELF";
                }
                other.GetComponent<PlayerHealth>().CalculateDmg(other.name, bulletFromPlayer, gameObject, GetComponent<SphereCollider>().radius);
            }
        }

    }
}
