﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Rocket : NetworkBehaviour {

    public GameObject explosionPrefab;

    [SyncVar]
    public string _fromPlayer = "";

    private void Start()
    {
        Destroy(gameObject, 10);
    }    

    // Use this for initialization
    private void OnCollisionEnter(Collision collision)
    {
        GameObject expl = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        expl.GetComponent<Explosion>().bulletFromPlayer = _fromPlayer;
        Destroy(gameObject, 0.1f);
    }
}
