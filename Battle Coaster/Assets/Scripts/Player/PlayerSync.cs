﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerSync : NetworkBehaviour
{
    [SyncVar(hook = "PlayerNameChanged")]
    string m_playerName; //has to be "" to sync correct

    [SyncVar(hook = "PlayerSkinChanged")]
    int m_playerSkin = -1; //has to be -1 to sync correct

    [SyncVar(hook = "PlayerKillsChanged")]
    public int m_playerKills = 0;

    //Scripts
    private BoardNames boardNames;

    public override void OnStartLocalPlayer()
    {
        CmdSetPlayerSkin(GameManager.Instance._playerSkin);
        CmdSetPlayerName(GameManager.Instance._playerName);
    }

    public override void OnStartClient()
    {
        if (!isServer)
        {
            if (isLocalPlayer)
            {
                return;
            }
            PlayerSkinChanged(m_playerSkin);
            PlayerNameChanged(m_playerName);
        }
    }

    [Command]
    void CmdSetPlayerName(string newName)
    {
        m_playerName = newName;
    }

    [Command]
    void CmdSetPlayerSkin(int newSkin)
    {
        m_playerSkin = newSkin;
    }

    [Command]
    public void CmdSetKills(int newKills)
    {
        m_playerKills = newKills;
    }

    void PlayerNameChanged(string newName)
    {
        if (isClient)
        {
            //GetComponentInChildren<TextMesh>().text = newName; //first time is ""
            m_playerName = newName;

            if (m_playerName != "")
            {
                //   GetComponentInChildren<TextMesh>().text = newName;

                GetComponent<PlayerSetup>().RegisterPlayer(newName);
            }
        }
    }

    void PlayerSkinChanged(int newSkin)
    {
        m_playerSkin = newSkin;
        if (m_playerSkin != -1)
        {
            GetComponent<PlayerSkin>().ChangeSkin(newSkin);
        }
    }

    void PlayerKillsChanged(int newKills)
    {
        m_playerKills = newKills;

        if (isLocalPlayer)
        {
            UIManager.Instance.SetMyKillsText(newKills);
        }
    }
}