﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerDetector : NetworkBehaviour {

    private const string ITEM_HEALTH_TAG = "Item_Health";

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(ITEM_HEALTH_TAG))
        {
            if (isLocalPlayer)
            {
                float healthAdded = other.GetComponent<Item_Health>().healthToAdd;
                GetComponent<PlayerHealth>().CmdAddHealth(healthAdded);
            }

            Destroy(other.gameObject);
        }
    }
}
