﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerSkin : NetworkBehaviour
{
    public RuntimeAnimatorController generalController;
    public GameObject[] skinPrefabs;
    public int currentSkin;

    private GameObject skinInstance;
    private Animator anim;

    public GameObject weapon;

    public void ChangeSkin(int skinN)
    {
        currentSkin = skinN;
        anim = GetComponent<Animator>();
        skinInstance = Instantiate(skinPrefabs[currentSkin], transform.position, transform.rotation, transform);
        skinInstance.transform.SetAsFirstSibling();
        skinInstance.name = "PlayerSkin";

        ChangeAnimator();
    }

    private void ChangeAnimator()
    {
        anim.runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Invector_BasicLocomotionLITE", typeof(RuntimeAnimatorController));
        anim.avatar = skinInstance.GetComponent<Animator>().avatar;
        anim.Rebind();

        //parent weapon to the joint
        weapon.transform.parent = anim.GetBoneTransform(HumanBodyBones.Head).transform;
    }
}
