﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnterNameUI : MonoBehaviour {

    public Text InputFieldText;
    public Text InputFieldErrorText;

    public void Login()
    {
        string userName = InputFieldText.text;

        if(userName.Length < 3)
        {
            InputFieldErrorText.text = "Name is too short! Minimum: 3 letters";
            return;
        }
        else if( userName.Length > 13)
        {
            InputFieldErrorText.text = "Name is too long! Maximum 13 letters";
            return;
        }

        GameManager.Instance.SetPlayerName(InputFieldText.text);
        SceneManager.LoadScene(1);
    }
}
