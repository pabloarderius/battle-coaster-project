﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [Header("GameObjects")]
    public GameObject uiEspectating;

    [Header("UI Text")]
    public Text infoEspectating;
    public Text killsText;
    public Text otherPlayerKillsText;
    public Text placedText;

    [Header("UI Scrips")]
    public BoardNames boardNames;
    public Crosshair crosshair;

    private void Awake() //this script is only in the Online Game Scene
    {
        Instance = this;
        uiEspectating.SetActive(false);
    }

    public void SetEspectatingText(string espectatingText)
    {
        uiEspectating.SetActive(true);
        infoEspectating.text = espectatingText;
        int playersLeft = GameManager.Instance.players.Count + 1;
        placedText.text = "You placed " + "<color=yellow>" + "#" + playersLeft + "</color>";
        crosshair.enabled = false;
    }

    public void SetOtherPlayerKills(int otherPKills)
    {
        otherPlayerKillsText.text = otherPKills.ToString();
    }

    public void SetMyKillsText(int kills)
    {
        killsText.text = kills.ToString();
    }

    public void SetGameObjectActive(GameObject go)
    {
        go.SetActive(!go.activeSelf);
    }

}