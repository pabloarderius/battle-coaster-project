﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    [SerializeField]
    GameObject pauseMenu;

    public static bool paused = false;

    private NetworkManager networkManager;

    private void Start()
    {
        networkManager = NetworkManager.singleton;
        paused = false;
        pauseMenu.SetActive(paused);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TooglePauseMenu();
        }
    }

    public void LeaveRoom()
    {
        MatchInfo matchInfo =  networkManager.matchInfo;
        networkManager.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, networkManager.OnDropConnection);
        networkManager.StopHost();
        StartCoroutine(TimeOutLeaveRoom());
    }

    IEnumerator TimeOutLeaveRoom() //Error of disconection of host
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);
    }

    private void TooglePauseMenu()
    {
        paused = !pauseMenu.activeSelf;
        pauseMenu.SetActive(paused);
        // Time.timeScale = (paused) ? 1 : 0;
    }
}
