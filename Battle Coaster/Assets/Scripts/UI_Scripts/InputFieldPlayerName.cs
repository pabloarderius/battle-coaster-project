﻿using UnityEngine;
using UnityEngine.UI;

public class InputFieldPlayerName : MonoBehaviour {

    InputField field;

    private void Start()
    {
       field = GetComponent<InputField>();
    }

    public void SetUpPlayerName()
    {
        GameManager.Instance.SetPlayerName(field.text);
    }
}
