﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    [Header("UI")]
    public AnimationCurve animationCurve;
    public Image healthBarGreen, healthBarRed;
    public Text healthText;

    public float health = 100f;

    [Header("Timing")]
    public float greenHBDuration = 0.5f;
    public float redHBDuration = 1f;

    public void ChangeHealthBar(float newHealth)
    {
        StartCoroutine(AnimateHealth(healthBarGreen, newHealth, greenHBDuration));
        StartCoroutine(AnimateHealth(healthBarRed, newHealth, redHBDuration));

        health = newHealth;
    }

    IEnumerator AnimateHealth(Image healthBar, float destHealth, float duration)
    {
        float originHealth = health / 100;
        destHealth = destHealth / 100;

        float elapsedTime = 0;
        while (elapsedTime < duration)
        {
            float percent = elapsedTime / duration;
            float curvePercent = animationCurve.Evaluate(percent);

            healthBar.fillAmount = Mathf.Lerp(originHealth, destHealth, curvePercent);
            int healthInt = (int)(healthBar.fillAmount * 100);
            healthText.text = healthInt + "%";
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    #region oldHealthAnim
    //IEnumerator HealthDamageAnim(Image healthBar, float damage, float duration)
    //{
    //    float originHealth = health / 100;
    //    float destHealth = (health / 100) - (damage / 100);

    //    float elapsedTime = 0;
    //    while (elapsedTime < duration)
    //    {
    //        float percent = elapsedTime / duration;
    //        float curvePercent = animationCurve.Evaluate(percent);

    //        healthBar.fillAmount = Mathf.Lerp(originHealth, destHealth, curvePercent);
    //        int healthInt = (int)(healthBar.fillAmount * 100);
    //        healthText.text = healthInt + "%";
    //        elapsedTime += Time.deltaTime;
    //        yield return new WaitForEndOfFrame();
    //    }
    //}

    //public void TakeDamage(float damage)
    //{
    //    StartCoroutine(HealthDamageAnim(healthBarGreen, damage, greenHBDuration));
    //    StartCoroutine(HealthDamageAnim(healthBarRed, damage, redHBDuration));
    //    health -= damage;
    //    health = Mathf.Max(0, health);
    //}
    #endregion
}
