﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardNames : MonoBehaviour {

    public Text boardNamesText;
    public Text playersLeftText;

    public void DisplayUserNames(Dictionary<string, Player> playersDict) //Display online user names on UI board
    {
        boardNamesText.text = "";

        foreach (var player in playersDict.Values)
        {
            boardNamesText.text += player.playerName + "\n";
        }
        playersLeftText.text = playersDict.Count.ToString();
    }

}
