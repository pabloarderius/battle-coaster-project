﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class NetworkMigration_Custom : NetworkMigrationManager
{

    protected override void OnClientDisconnectedFromHost(NetworkConnection conn, out SceneChangeOption sceneChange)
    {
        base.OnClientDisconnectedFromHost(conn, out sceneChange);

        NetworkLobbyManager manager = GetComponent<NetworkLobbyManager>();

        PeerInfoMessage peerInfo;
        bool newHost;

        FindNewHost(out peerInfo, out newHost);

        if (newHost)
        {
            BecomeNewHost(manager.networkPort);
        }
    }
}
