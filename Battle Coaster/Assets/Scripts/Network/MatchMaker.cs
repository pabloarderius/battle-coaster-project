﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class MatchMaker : MonoBehaviour {

    //Create Room
    [SerializeField]
    private uint roomSize = 15;
    private string roomName;
    [SerializeField]
    private Text createMatchStatus;
    private NetworkManager networkManager;

    //Join Room
    private List<GameObject> roomList = new List<GameObject>();
    [SerializeField]
    private Text status;
    [SerializeField]
    private GameObject roomItemPrefab;
    [SerializeField]
    private Transform roomListParent;

    private void Start()
    {
        networkManager = NetworkManager.singleton;
        if(networkManager.matchMaker == null)
        {
            networkManager.StartMatchMaker();
        }

        RefreshRoomList();
    }


    #region HOST_MATCH

    public void SetRoomName(string _name)
    {
        roomName = _name;
    }

	public void CreateRoom()
    {
        if(roomName != "" &&  roomName != null)
        {
            Debug.Log("Creating room: " + roomName + " with room for " + roomSize + "players.");
            //Create room
            networkManager.matchMaker.CreateMatch(roomName, roomSize, true, "", "", "", 0, 0, networkManager.OnMatchCreate);
            createMatchStatus.text = "Creating match... Please wait...";
        }
    }

    #endregion

    #region JOIN_MATCH

    public void RefreshRoomList()
    {
        ClearRoomList();
        networkManager.matchMaker.ListMatches(0, 20, "", false, 0, 0, OnMatchList);
        status.text = "Loading...";
    }

    public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        status.text = "";
        if(matchList == null)
        {
            status.text = "Couldn't get room list.";
            return;
        }
        foreach (MatchInfoSnapshot match in matchList)
        {
            GameObject _roomListItemGO = Instantiate(roomItemPrefab, roomListParent);
            //Have a component set on the gameobject 
            //that will take care of setting up the name/amount of users
            //as well as setting up a callback function that will join the game

            RoomListItem roomListItem = _roomListItemGO.GetComponent<RoomListItem>();
            if(roomListItem != null)
            {
                roomListItem.Setup(match,JoinRoom);
            }
            roomList.Add(_roomListItemGO);

        }

        if (roomList.Count == 0)
        {
            status.text = "No rooms at the moment.";
        }
    }

    private void ClearRoomList()
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            Destroy(roomList[i]);
        }

        roomList.Clear();
    }

    public void JoinRoom(MatchInfoSnapshot _match)
    {
        Debug.Log("joining " + _match.name);
        networkManager.matchMaker.JoinMatch(_match.networkId, "", "", "", 0, 0, networkManager.OnMatchJoined);
    }

    #endregion
}
