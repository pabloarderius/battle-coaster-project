﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    [Header("Scripts")]
    public static GameManager Instance;
    private BoardNames boardNames;

    [Header("Player Properties")]
    public string _playerName = "";
    public int _playerSkin = 0;

    [Header("Player Network ID")]
    private const string PLAYER_ID_PREFIX = "Player ";
    public Dictionary<string, Player> players = new Dictionary<string, Player>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            QualitySettings.SetQualityLevel(3);
            return;
        }
        if (Instance == this) { return; }
        Destroy(this.gameObject);
    }

    public void RegisterPlayer(string _netID, Player _player) //Player connected
    {
        string _playerID = PLAYER_ID_PREFIX + _netID;
        players.Add(_playerID, _player);
        _player.transform.name = _playerID;

        if(boardNames == null)
        {
            boardNames = UIManager.Instance.boardNames;
        }

        boardNames.DisplayUserNames(players);
    }

    public void UnRegisterPlayer(string _playerID) //Player disconnected
    {
        players.Remove(_playerID);
        boardNames.DisplayUserNames(players);
    }

    public Player GetPlayer (string _playerID)
    {
        return players[_playerID];
    }

    public Player GetPlayerByDictionaryPos (int dictPos)
    {
        if(players.Count >= dictPos)
        {
            Player otherPlayer = players.ElementAt(dictPos).Value; //Gets the first player (or the id who killed me)
            return otherPlayer;
        }
        else
        {
            return players.ElementAt(0).Value; //if trying to get null player, get the first
        }

    }

    public void SetPlayerName(string name)
    {
        _playerName = name;
    }

    public void SetPlayerSkin(int skin)
    {
        _playerSkin = skin;
    }
}
