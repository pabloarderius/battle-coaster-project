﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSelection : MonoBehaviour {

    public int currentPlayerId = 0;
    public GameObject playerInGamePrefab;
    public GameObject particleSelection;
    public GameObject lightSelection;

    void Update () {

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 20))
        {
            if(hit.collider.tag == "Player")
            {
                Vector3 particlePos = particleSelection.transform.position;
                particleSelection.transform.position = new Vector3(hit.collider.transform.position.x,particlePos.y, particlePos.z);
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                PlayerSelectionID selectionId = hit.collider.GetComponent<PlayerSelectionID>();
                if (selectionId != null)
                {
                    currentPlayerId = selectionId.playerId;
                    //playerInGamePrefab.GetComponent<PlayerSkin>().currentSkin = currentPlayerId;
                    GameManager.Instance.SetPlayerSkin(currentPlayerId);

                    Vector3 lightPos = lightSelection.transform.position;
                    lightSelection.transform.position = new Vector3(hit.collider.transform.position.x, lightPos.y, lightPos.z);
                }
            }
        }
	}
}
